install.packages('ggplot2')
library("ggplot2")

install.packages('ggplot2movies')
library(ggplot2movies) #data set
str(movies)
pl <- ggplot(movies, aes(x = rating)) #aes = �����
#���� ������ �� ����� ��"� ������ �� �� ����� ����

#����� �� ����� ����������
#���� ����� ��������
pl <- pl + geom_histogram(binwidth = 0.5, color = 'blue', fill = 'red', alpha = 0.2)
pl <- pl + ggtitle('A histogram of movie rating') #����� �����

pl1 <- ggplot(movies, aes(x = length, y = rating))
pl1 <- pl1 + geo_point() #��� ������
#���� ������, ����� ������
pl1 <- pl1 + xlim(0, 250) #����� ������ ��� �����
#�� ����� ����� ����� ����� ���� ���� ����� ���� ����

pl2 <- ggplot(movies, aes(x = year, y = rating))
pl2 <- pl2 + geo_bin2d() + scale_fill_gradient(low = 'green', high = 'red') #��� ���

str(mtcars) #����� �� R
#��� ������ ��� ����� ����� �� ���� ���������
pl3 <- ggplot(mtcars , aes(x = wt, y = mpg))
pl3 <- pl3 + geo_point(aes(shape = factor(cyl)), size = 5) #cyl �������
#����� �� ����� ���� �����

str(mpg) #cars data set

pl4 <- ggplot(mpg, aes(x = class))
pl4 <- pl4 + geom_bar(color = 'blue', fill = 'light blue')
pl4 <- pl4 + geom_bar(aes(fill = drv), position = 'fill') #����� �����
# 100 ����

#####################################################
#bike
bikes <- read.csv("train.csv", sep=",")
str(bikes)

#���� ����� ����� �� ��� ������, ������ ����� ���� �����
bikes$datetime <- as.character(bikes$datetime)
bikes$date <- sapply(strsplit(bikes$datetime, ' '), "[",1) #������ ��� �����,1 ���� ������ ����� ���� �� ���� ������ �� ������
bikes$date <- as.Date(bikes$date)

bikes$time <- sapply(strsplit(bikes$datetime, ' '), "[",2)
bikes$hour <- sapply(strsplit(bikes$time, ':'), "[",1)
bikes$hour <- as.numeric(bikes$hour)
#������ ����� ��� ���� �� ���� ����� ���� ������� ��������