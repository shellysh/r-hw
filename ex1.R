#Load the data
library(datasets)
data(airquality)
head(airquality)

#Q1 - Calculate the number on rows with NA�s
Q1 <- sum(is.na(airquality))

#Q2 - Remove all the rows with NA�s
Q2 <- na.omit(airquality)

#Q3 - Produce a vector with all average measures (Ozone Solar.R Wind Temp)
Q3 <- c(mean(Q2$Ozone), mean(Q2$Solar.R), mean(Q2$Wind), mean(Q2$Temp))

#Q4 - Filter the dataset for rows that are above average in temperature 
Q4 <- Q2[mean(Q2$Temp)<Q2]

#Q5 - Sort the dataset according to solar radiation 
Q5 <- Q2[order(Q2$Solar.R),]

#Q6 - Add a column to the dataset that shows a ratio between Ozone and Solar.R (use the apply function properly) 
Q2$ratio <- Q2$Ozone/Q2$Solar.R
Q2

#Q7 - Plot on the same page all 6 graphs of one attribute against another (e.g. Ozone against Solar.R). From the graphs which variables seem related? 
par(mfrow = c(6,1))
plot(Q2$Ozone, Q2$Solar.R, main="Ozone vs Solar. R")
plot(Q2$Ozone, Q2$Wind, main="Ozone vs Wind")
plot(Q2$Ozone, Q2$Temp, main="Ozone vs Temp")
plot(Q2$Solar.R, Q2$Wind, main="Solar. R vs Wind")
plot(Q2$Solar.R, Q2$Temp, main="Ozone vs Temp")
plot(Q2$Wind, Q2$Temp, main="Wind vs Temp")
#Ozone vs Wind & Ozone vs Temp look related
