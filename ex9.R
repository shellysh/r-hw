install.packages('randomForest')
library(randomForest)
spam  <- read.csv('spam.csv', stringsAsFactors = FALSE)
library(tm) #textmining
library(wordcloud)

spam$type <- as.factor(spam$type)
str(spam)

spam_corpus <- Corpus(VectorSource(spam$text))
clean_corpus <- tm_map(spam_corpus, removePunctuation) #���� ����� ������
clean_corpus <- tm_map(clean_corpus, stripWhitespace) #������ �� ����� ���� �� ��� ������
clean_corpus <- tm_map(clean_corpus, content_transformer(tolower)) #����� ������ ������ ����
clean_corpus <- tm_map(clean_corpus, removeWords, stopwords())
clean_corpus <- tm_map(clean_corpus, stripWhitespace)

dtm <- DocumentTermMatrix(clean_corpus) 
dim(dtm)

frequent_dtm <- DocumentTermMatrix(clean_corpus, list(dictionary = findFreqTerms(dtm, 10))) #����� ������ ������ ������ ����� ������� �� ���� ��� ������� ���� ���� �10
dim(frequent_dtm)

#--------------------------------------------------------------------
#Naive Bais
#Spliting into training set and test set
split <- runif(500) #����� ���� ������� �����, 500 ����� ����� �������

split <- split > 0.3 #����� �30-70

#dividing raw data
train_raw <- spam[split,] #raw �����
test_raw <- spam[!split,]

#dividing clean corpus
train_corpus <- clean_corpus[split]
test_corpus <- clean_corpus[!split]

#dividing dtm
train_dtm <- frequent_dtm[split,]
test_dtm <- frequent_dtm[!split,]

#Convert the DTM into yes/no
conv_yesno <- function(x){
  x <- ifelse(x > 0, 1, 0)
  x <- factor(x, levels = c(1,0), labels = c('Yes','No'))
}

train <- apply(train_dtm, MARGIN = 1:2, conv_yesno) #Marging �� �� ����� ����� ������ �� �������� ������ ����� �� �� ���, ���� ������ �� ���
test <- apply(test_dtm, MARGIN = 1:2, conv_yesno)

#converting into data frame
df_train <- as.data.frame(train)
df_test <- as.data.frame(test)

#Add the type column
df_train$type <- train_raw$type
df_test$type <- test_raw$type

dim(df_train) #type = ����� 58

#Generating the model using naive bayes
library(e1071)
model <- naiveBayes(df_train[,-58], df_train$type) #���� ����� ��� ����� 58 �� ����� ����� ���� ����� 58

prediction <- predict(model, df_test[,-58])

install.packages('SDMTools') #Confusion matrixs
library(SDMTools)

#������� ���� ��������� �� ���� ������ �� ���� ��� ������ - 0 �1
convert_01 <- function(x){
  x <- ifelse(x == 'spam', 1, 0)
}

pred_01 <- sapply(prediction, convert_01)
actual <- sapply(df_test$type, convert_01)

confusion <- confusion.matrix(actual, pred_01) #�� ������ ����

TP <- confusion[2,2] #1,1
FP <- confusion[2,1] #1,0
TN <- confusion[1,1] #0,0
FN <- confusion[1,2] #0,1

recall <- TP/(TP+FN) #��� �� ��� ��� ������� ��� ��� ������ ����
precision <- TP/(TP+FP) #���� �� ��� ��� ������� ��� ��� ���� �����

model.nb <- naiveBayes(df_train[,-58], df_train$type)

prediction.nb <- predict(model.nb,df_test[,-58])

#see the probability
predictedprob.nb <- predict(model.nb,model.nb$type, type ='prob')
#see only spam records
predictedprob.rf[,'spam']

#building roc chart
rocCurve.nb <- roc(type$Sapm, predictedprob.nb[,'spam'], levels = c("ham","spam"))
plot(rocCurve, col="yellow", main='ROC chart')

#---------------------------------------------------------------------
#Decision Tree
install.packages('rpart')
library(rpart)
install.packages('rpart.plot')
library(rpart.plot)

tree <-rpart(type ~ ., spam) #���� �������� �� �� ������
prp(tree) #���� ��������� �� ���

predicted_tree <- predict(tree, spam) #�� ������ �� �������, ���� ���� ������� �� �� ������

#����� �� ��������
predProb_tree <- predict(tree ,spam, type ='prob')
#����� �� �� ������� �� ����
predProb_tree[,'spam']

#ROC curve
install.packages('pROC')
library(pROC)

#roc chart
rocCurve_tree <- roc(spam$type, predProb_tree[,'spam'], levels = c("ham","spam"))
plot(rocCurve_tree, col = "yellow", main = "Roc Chart")

#Perfect match

#-------------------------------------------------------
#Random Forest
install.packages('randomForest')
library(randomForest)
any(is.na(spam))
as.numeric(spam$type)
rf.model <- randomForest(spam$type ~ ., data = spam)

print(rf.model)

predicted <-predict(rf.model, spam)

#�� ���� ����� �� ����������
predictedProb <- predict(rf.model, spam, type = 'prob')
predictedProb[, 'spam'] #����� ����� �� ��� ���������� �� ���� ���

#ROC curve
#absent no positive, present positive
rocCurve <- roc(spam$type, predictedProb[, 'present'], levels = c("ham","spam"))
#we get area (AVC) = 1
plot(rocCurve, col = "yellow", main = "Roc Chart")
